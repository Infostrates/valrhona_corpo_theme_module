import $ from 'jquery';
import 'foundation-sites/dist/js/foundation';

import gsap from 'gsap';
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { SplitText } from  'gsap/SplitText'
gsap.registerPlugin(SplitText, ScrollTrigger);

import 'slick-carousel'
import 'slick-carousel/slick/slick.scss'
import 'slick-carousel/slick/slick-theme.scss'

const backLink = $('#back-menu');
function goBack(){
    $('.has-sub-menu').removeClass('has-open-sub-menu');
    backLink.removeClass('active');
}

$(function (){
    setTimeout(function (){

        backLink.on('click', function(){goBack()});

        // Défilement des évènements
        if ($('#event-banner').length > 0)  {
            $('#event-banner .cell:last-child').slick({
                arrows: false,
                fade: true,
                autoplay: true,
                speed: 300,
                autoplaySpeed: 4000
            });
        } else {
            // Si pas d'event, on ajuste le margin-top du corps du site
            if (Foundation.MediaQuery.atLeast('large') && $('header.opaque').length > 0) {
                $('main').css('margin-top', $('header').outerHeight());
            }
        }

        // Fermer les différents menus et éléments d'info avant d'en ouvrir un (autre...menu)
        $('#localizationToggle, #menuToggle')
            .on('on.zf.toggler', function (){
                $('#event-banner').addClass('hidden-up');
                $('body').addClass('menu-open');
            })
            .on('off.zf.toggler', function(){
                backLink.removeClass('active');
                $('body').removeClass('menu-open');
            });

        let utils = {

            generateID: function (base) {
                return base + Math.floor(Math.random() * 999);
            },

            focusIsInside(element) {
                return element.has(document.activeElement).length;
            }

        };

        function hideDropdown() {
            $('header').removeClass('open-sub-menu');
            $('#main-menu > li.has-sub-menu').removeClass('has-open-sub-menu');
            backLink.removeClass('active');
        }

        function toggleDropdown(e) {
            if($(e).parent().hasClass('has-open-sub-menu')) {
                $('header').removeClass('open-sub-menu');
                $('#main-menu > li.has-sub-menu').removeClass('has-open-sub-menu');
                backLink.removeClass('active');
            } else {
                $('header').addClass('open-sub-menu');
                $('#main-menu > li.has-sub-menu').removeClass('has-open-sub-menu');
                $(e).parent().addClass('has-open-sub-menu');
                backLink.addClass('active')
            }
        }

        function collapseDropdownsWhenClickingOutsideNav(e) {
            let target = e.target;

            if (!$('#main-menu > li.has-sub-menu').has(target).length) {
                hideDropdown(this);
            }
        }

        function collapseDropdownsWhenTabbingOutsideNav(e) {
            if (e.keyCode === 9 && utils.focusIsInside($('#main-menu > li.has-open-sub-menu')) === 0 ) {
                toggleDropdown(this);
            }
        }

        $('#main-menu > li.has-sub-menu button').on('click touch', function (e) {
            toggleDropdown(this);
        });

        $('#main-menu > li[role="button"].has-sub-menu').on("keydown", function(e) {
            if (e.keyCode === 13) {
                toggleDropdown(this);
            }
        });

        $('#main-menu > li[role="button"].has-sub-menu').on("keydown", function(e) {
            if (e.keyCode === 32) {
                e.preventDefault();
            }
        });

        $('#main-menu > li[role="button"].has-sub-menu').on("keyup", function(e) {
            if (e.keyCode === 32) {
                toggleDropdown(this);
            }
        });

        document.addEventListener("keyup", collapseDropdownsWhenTabbingOutsideNav);
        window.addEventListener("click", collapseDropdownsWhenClickingOutsideNav);

        // Prevent du bounce scroll du header (safari mobile surtout)
        $('header .grid-container:not(:last-child) > .grid-x > .cell:not(.small-12)').on('touchmove', function (e) {
            e.preventDefault();
        });

        // À la sortie du menu au scroll sur iphone, retirer la flèche
        $('.sub-menu-wrapper').on('touchmove', function(event){
            let y = event.touches[0].clientY,
                outZone = $('header').outerHeight();
            if (outZone > y) {
                goBack();
            }
        });

        // Script effect menu
        $('header .sub-menu a').each(function () {
            let hoverEffect = gsap.timeline({paused: true}),
                linkLines = new SplitText($(this), { type: 'lines'});
            hoverEffect
                .to(linkLines.lines, {duration: 0.1, alpha: 0, width: '100%'})
                .set(linkLines.lines, { alpha: 1, width: '0%', fontFamily: 'Belluga', fontWeight: 'normal', fontSize: '20px', lineHeight: '34px', overflow: 'hidden', whiteSpace: 'nowrap', padding: 0})
                .to(linkLines.lines, { delay: 0.1, duration: 0.6, stagger: 0.35, width: '100%'})
            ;

            $(this).on('mouseover touch', function(){
                if(Foundation.MediaQuery.atLeast('large')) {
                    hoverEffect.play();
                }
            }).on('mouseout touchend', function(){
                if(Foundation.MediaQuery.atLeast('large')) {
                    hoverEffect.reverse();
                }
            });
        });

        // pas de transition entre les sous menus
        let subMenus = $('.grid-x.sub-menu-wrapper'),
            menuHeight = 0;

        subMenus.each(function (){
            if($(this).outerHeight() > menuHeight ){
                menuHeight = $(this).outerHeight();
            }
        });
        subMenus.css('height', menuHeight);

        // Header compact au scroll
        const eltHeader = $('header');
        eltHeader.addClass('fixed');
        gsap.to("body", {
            scrollTrigger: {
                trigger: "body",
                start: "top top",
                toggleClass: { targets: eltHeader, className: "compact" },
            }
        });
    }, 0);
});