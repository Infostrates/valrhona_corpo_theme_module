import '../scss/common.scss';
import '../scss/layout.scss';

import $ from 'jquery';
import 'foundation-sites/dist/js/foundation';

import './layout';

$(document).foundation();

$(function (){

    // debug data-toggler safari mobile
    $('[data-toggle]').on('click', function () {
        // Ce commentaire ne fait rien mais débug le data-toggle sur Safari mobile. Ne le supprimez donc pas.
    });

    // Inline des svg .inline seulement quand l'image est loaded
    let svgToInline = $('.inline-svg, #logo img');
    svgToInline.each(function () {
        let svgImg = $(this),
            svgUrl = svgImg.attr('src'),
            isNowInlined = new Event('isNowInlined');
        Foundation.onImagesLoaded(svgImg, function () {
            $.get(svgUrl, function (data) {
                let svgContent = $(data).find('svg');
                svgContent.removeAttr('xmlns:a');
                svgImg.replaceWith(svgContent);
                svgImg[0].dispatchEvent(isNowInlined);
            }, 'xml');
        });
    })

})