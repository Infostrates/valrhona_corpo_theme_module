# README #

Tout est décrit ici https://wiki.infostrates.fr/books/infra/page/utilisation-du-header-footer-valrhona

## OLD - Dans mon projet
* Dans package.json : Retirer Foundation des dépendances directes du projet
* dans app.js :
  ```import 'valrhona_corpo_theme_module/js/common.js' ```
* dans les fichiers scss :
``` @import '~valrhona_corpo_theme_module/scss/settings';```
``` @import '~valrhona_corpo_theme_module/scss/mixins_is';```
``` @import '~foundation-sites/scss/foundation'; ```

## OLD - Known Issues :'(
* La version intégrée de GSAP ne s'installe pas ?
Faire ```npm install node_modules/valrhona_corpo_theme_module/gsap-bonus_3.5.0.tgz``` 
